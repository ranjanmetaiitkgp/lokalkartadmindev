<%@ include file="/common/taglibs.jsp"%>

<head>
    <title><fmt:message key="post.content.create"/></title>
    <meta name="menu" content="UserMenu"/>
</head>

<div class="col-sm-7">
    <spring:bind path="forumPost.*">
        <c:if test="${not empty status.errorMessages}">
        <div class="alert alert-danger alert-dismissable">
            <a href="#" data-dismiss="alert" class="close">&times;</a>
            <c:forEach var="error" items="${status.errorMessages}">
                <c:out value="${error}" escapeXml="false"/><br />
            </c:forEach>
        </div>
        </c:if>
    </spring:bind>

    <form:form commandName="forumPost" method="post" action="forumpostform" onsubmit="return validateForumPost(this)" id="forumpostform" cssClass="well">
    	<input type="hidden" name="id" value="<c:out value="${param.id}"/>"/>
        <spring:bind path="forumPost.content">
        <div class="form-group${(not empty status.errorMessage) ? ' has-error' : ''}">
        </spring:bind>
            <appfuse:label key="post.content.create" styleClass="control-label"/>
            <form:input cssClass="form-control" path="content" id="content"/>
            <form:errors path="content" cssClass="help-block"/>
        </div>
        
        <div class="form-group">
            <button type="submit" name="save" class="btn btn-primary" onclick="bCancel=false">
                <i class="icon-upload icon-white"></i> <fmt:message key="button.post"/>
            </button>
            <button type="submit" name="cancel" class="btn btn-default" onclick="bCancel=true">
                <i class="icon-remove"></i> <fmt:message key="button.cancel"/>
            </button>
        </div>
    </form:form>
    
    <display:table name="forumPostList" cellspacing="0" cellpadding="0" requestURI=""
                   defaultsort="1" id="forumPostList" pagesize="25" class="table table-condensed table-striped table-hover" export="true">
		<display:column property="contentWriter.username" escapeXml="true" sortable="true" titleKey="label.username" style="width: 25%"/>
		<display:column property="content" escapeXml="true" sortable="true" titleKey="post.content" style="width: 75%"/>
    </display:table>
</div>
