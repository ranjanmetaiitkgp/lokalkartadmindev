<%@ include file="/common/taglibs.jsp"%>

<head>
    <title><fmt:message key="upload.title"/></title>
    <meta name="menu" content="UserMenu"/>
</head>

<div class="col-sm-7">
    <spring:bind path="circle.*">
        <c:if test="${not empty status.errorMessages}">
        <div class="alert alert-danger alert-dismissable">
            <a href="#" data-dismiss="alert" class="close">&times;</a>
            <c:forEach var="error" items="${status.errorMessages}">
                <c:out value="${error}" escapeXml="false"/><br />
            </c:forEach>
        </div>
        </c:if>
    </spring:bind>

    <form:form commandName="circle" method="post" action="circleform" onsubmit="return validateCircle(this)" id="circleform" cssClass="well">
        <spring:bind path="circle.circleName">
        <div class="form-group${(not empty status.errorMessage) ? ' has-error' : ''}">
        </spring:bind>
            <appfuse:label key="circle.circleName" styleClass="control-label"/>
            <form:input cssClass="form-control" path="circleName" id="circleName"/>
            <form:errors path="circleName" cssClass="help-block"/>
        </div>
        
        <div class="form-group">
            <button type="submit" name="save" class="btn btn-primary" onclick="bCancel=false">
                <i class="icon-upload icon-white"></i> <fmt:message key="button.save"/>
            </button>
            <button type="submit" name="cancel" class="btn btn-default" onclick="bCancel=true">
                <i class="icon-remove"></i> <fmt:message key="button.cancel"/>
            </button>
        </div>
    </form:form>
    
    <display:table name="circleList" cellspacing="0" cellpadding="0" requestURI=""
                   defaultsort="1" id="users" pagesize="25" class="table table-condensed table-striped table-hover" export="true">
        <display:column property="circleName" escapeXml="true" sortable="true" titleKey="circle.circleName" style="width: 25%"
                        url="#" paramId="id" paramProperty="id"/>
    </display:table>
</div>
