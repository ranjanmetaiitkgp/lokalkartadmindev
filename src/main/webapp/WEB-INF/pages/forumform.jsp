<%@ include file="/common/taglibs.jsp"%>

<head>
    <title><fmt:message key="forum.form"/></title>
    <meta name="menu" content="UserMenu"/>
</head>

<div class="col-sm-7">
    <spring:bind path="forum.*">
        <c:if test="${not empty status.errorMessages}">
        <div class="alert alert-danger alert-dismissable">
            <a href="#" data-dismiss="alert" class="close">&times;</a>
            <c:forEach var="error" items="${status.errorMessages}">
                <c:out value="${error}" escapeXml="false"/><br />
            </c:forEach>
        </div>
        </c:if>
    </spring:bind>

    <form:form commandName="forum" method="post" action="forumform" onsubmit="return validateCircle(this)" id="forumform" cssClass="well">
        <spring:bind path="forum.forumName">
        <div class="form-group${(not empty status.errorMessage) ? ' has-error' : ''}">
        </spring:bind>
            <appfuse:label key="forum.forumName" styleClass="control-label"/>
            <form:input cssClass="form-control" path="forumName" id="forumName"/>
            <form:errors path="forumName" cssClass="help-block"/>
        </div>
        
        <div class="form-group">
            <button type="submit" name="save" class="btn btn-primary" onclick="bCancel=false">
                <i class="icon-upload icon-white"></i> <fmt:message key="button.save"/>
            </button>
            <button type="submit" name="cancel" class="btn btn-default" onclick="bCancel=true">
                <i class="icon-remove"></i> <fmt:message key="button.cancel"/>
            </button>
        </div>
    </form:form>
    
    <display:table name="forumList" cellspacing="0" cellpadding="0" requestURI=""
                   defaultsort="1" id="forums" pagesize="25" class="table table-condensed table-striped table-hover" export="true">
        <display:column property="forumName" escapeXml="true" sortable="true" titleKey="forum.forumName" style="width: 25%"
                        url="/forumpostform" paramId="id" paramProperty="id"/>
    </display:table>
</div>
