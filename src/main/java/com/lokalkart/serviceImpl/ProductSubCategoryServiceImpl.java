package com.lokalkart.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lokalkart.domain.ProductSubCategory;
import com.lokalkart.repository.ProductSubCategoryRepository;
import com.lokalkart.service.ProductSubCategoryService;

@Service
public class ProductSubCategoryServiceImpl implements ProductSubCategoryService{

	@Autowired
	private ProductSubCategoryRepository productSubCategoryRepository;
	
	@Override
	public ProductSubCategory saveProductSubCategory(ProductSubCategory productSubCategory) {
		return productSubCategoryRepository.save(productSubCategory);
	}

	@Override
	public ProductSubCategory findBySubCatId(String subCatId) {
		return productSubCategoryRepository.findOne(subCatId);
	}

}
