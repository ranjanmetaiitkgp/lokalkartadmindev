package com.lokalkart.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lokalkart.domain.Product;
import com.lokalkart.domain.Retailer;
import com.lokalkart.repository.ProductRepository;
import com.lokalkart.service.ProductService;

@Service
public class ProductServiceImpl implements ProductService{

	@Autowired
	private ProductRepository productRepository;
	
	@Override
	public List<Product> getProductListByRetailer(Retailer retailer) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Product saveProduct(Product product) {
		return productRepository.save(product);
	}

}
