package com.lokalkart.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lokalkart.domain.State;
import com.lokalkart.repository.StateRepository;
import com.lokalkart.service.StateService;

@Service
public class StateServiceImpl implements StateService{
	
	@Autowired
	private StateRepository stateRepository;

	@Override
	public List<State> getAllStateByCountry(String countryId) {
		return stateRepository.findAllStateByCountry(countryId);
	}

	@Override
	public State saveState(State state) {
		return stateRepository.save(state);
	}

	@Override
	public State getStateById(String stateId) {
		return stateRepository.findOne(stateId);
	}

}
