package com.lokalkart.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lokalkart.domain.Product;
import com.lokalkart.domain.Retailer;
import com.lokalkart.repository.RetailerRepository;
import com.lokalkart.service.RetailerService;

@Service
public class RetailerServiceImpl implements RetailerService{
	
	@Autowired
	private RetailerRepository retailerRepository;

	@Override
	public List<Retailer> getAllRetailerForProductByLocation(Product product, String pinCode) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Retailer saveRetailer(Retailer retailer) {
		return retailerRepository.save(retailer);
	}

	@Override
	public Retailer findByRetailerId(String retailerId){
		return retailerRepository.findOne(retailerId);
	}
}
