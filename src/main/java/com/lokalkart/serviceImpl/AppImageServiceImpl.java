package com.lokalkart.serviceImpl;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lokalkart.domain.AppImage;
import com.lokalkart.repository.AppImageRepository;
import com.lokalkart.service.AppImageService;

@Service
public class AppImageServiceImpl implements AppImageService{

	@Autowired
	private AppImageRepository appImageRepository;
	
	@Override
	public AppImage saveAppImage(AppImage appImage) {
		return appImageRepository.save(appImage);
	}
	
	public AppImage findById(String id) {
		return appImageRepository.findOne(id);
	}
}
