package com.lokalkart.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lokalkart.domain.Address;
import com.lokalkart.repository.StoreLocatorRepository;
import com.lokalkart.service.StoreLocatorService;

@Service
public class StoreLocatorServiceImpl implements StoreLocatorService{

	@Autowired
	private StoreLocatorRepository storeLocatorRepository;
	
	@Override
	public Address saveAddress(Address address) {
		return storeLocatorRepository.save(address);
	}

}
