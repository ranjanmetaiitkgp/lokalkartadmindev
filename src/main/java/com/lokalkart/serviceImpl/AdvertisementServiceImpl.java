package com.lokalkart.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lokalkart.domain.Advertisement;
import com.lokalkart.domain.ProductSubCategory;
import com.lokalkart.domain.Retailer;
import com.lokalkart.repository.AdvertisementRepository;
import com.lokalkart.service.AdvertisementService;

@Service
public class AdvertisementServiceImpl  implements AdvertisementService{

	@Autowired
	private AdvertisementRepository advertisementRepository;
	
	@Override
	public List<Advertisement> getAdsByRetailer(Retailer retailer) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Advertisement> getAdsByLocation(Long pinCode) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Advertisement> getAdsByProductSubCategory(ProductSubCategory productSubCategory) {
		// TODO Auto-generated method stub
		return null;
	}

}
