package com.lokalkart.domain.jsonelement;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

public class ProductCategoryJSON implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8312392952345982802L;
	
	private String catId;
	private String categoryName;
	private List<AppImageJSON> productCategoryImage;
	private Set<ProductSubCategoryJSON> subCategories;
	public String getCatId() {
		return catId;
	}
	public void setCatId(String catId) {
		this.catId = catId;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public List<AppImageJSON> getProductCategoryImage() {
		return productCategoryImage;
	}
	public void setProductCategoryImage(List<AppImageJSON> productCategoryImage) {
		this.productCategoryImage = productCategoryImage;
	}
	public Set<ProductSubCategoryJSON> getSubCategories() {
		return subCategories;
	}
	public void setSubCategories(Set<ProductSubCategoryJSON> subCategories) {
		this.subCategories = subCategories;
	}
	
	

}
