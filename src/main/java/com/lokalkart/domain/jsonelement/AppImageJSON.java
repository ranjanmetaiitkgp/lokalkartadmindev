package com.lokalkart.domain.jsonelement;

import java.io.Serializable;

import com.lokalkart.domain.ProductImageSize;

public class AppImageJSON implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1657427057644227659L;

	private String id;
	
	private String imageUrl;
	
	private ProductImageSize imageSize;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public ProductImageSize getImageSize() {
		return imageSize;
	}

	public void setImageSize(ProductImageSize imageSize) {
		this.imageSize = imageSize;
	}
}
