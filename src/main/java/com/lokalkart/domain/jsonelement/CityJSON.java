package com.lokalkart.domain.jsonelement;

import java.io.Serializable;

public class CityJSON implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8202896252119825390L;

	private String cityId;
	
	private String cityName;
	
	private String stateId;

	public String getCityId() {
		return cityId;
	}

	public void setCityId(String cityId) {
		this.cityId = cityId;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getStateId() {
		return stateId;
	}

	public void setStateId(String stateId) {
		this.stateId = stateId;
	}

}
