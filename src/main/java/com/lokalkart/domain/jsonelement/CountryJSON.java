package com.lokalkart.domain.jsonelement;

import java.io.Serializable;

public class CountryJSON implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6593556664686843490L;

	private String countryId;
	
	private String countryName;
	
	private String countryCode;

	public String getCountryId() {
		return countryId;
	}

	public void setCountryId(String countryId) {
		this.countryId = countryId;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
}
