package com.lokalkart.domain.jsonelement;

import java.io.Serializable;
import java.util.List;

public class RetailerJSON implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3475632347290818993L;
	
	private String retailerId;
	private String retailerName;
	private List<AddressJSON> address;
	
	public String getRetailerId() {
		return retailerId;
	}
	public void setRetailerId(String retailerId) {
		this.retailerId = retailerId;
	}
	public String getRetailerName() {
		return retailerName;
	}
	public void setRetailerName(String retailerName) {
		this.retailerName = retailerName;
	}
	public List<AddressJSON> getAddress() {
		return address;
	}
	public void setAddress(List<AddressJSON> address) {
		this.address = address;
	}

}
