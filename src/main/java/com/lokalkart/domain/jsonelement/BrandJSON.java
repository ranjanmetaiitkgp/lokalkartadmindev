package com.lokalkart.domain.jsonelement;

import java.util.List;
import java.util.Set;

public class BrandJSON {
	
	private String brandId;
	private String brandName;
	private List<AppImageJSON> productImages;
	
	public String getBrandId() {
		return brandId;
	}
	public List<AppImageJSON> getProductImages() {
		return productImages;
	}
	public void setProductImages(List<AppImageJSON> productImages) {
		this.productImages = productImages;
	}
	public void setBrandId(String brandId) {
		this.brandId = brandId;
	}
	public String getBrandName() {
		return brandName;
	}
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}
	private Set<ProductJSON> products;
	public Set<ProductJSON> getProducts() {
		return products;
	}
	public void setProducts(Set<ProductJSON> products) {
		this.products = products;
	}
}
