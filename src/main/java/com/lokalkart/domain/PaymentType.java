package com.lokalkart.domain;

/**
 * 
 * @author Rohit Ranjan
 *
 */
public enum PaymentType {
	
	COD, CREDITCARD, NETBANKING, DEBITCARD;

}
