package com.lokalkart.domain;

import java.io.Serializable;
import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * 
 * @author Rohit Ranjan
 *
 */
@Document
public class UserSession implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8382556651018769680L;
	
	@Id
	private String sessionId;
	private Date creationTime;
	private Date loginTime;
	private Date logoutTime;
	
	@DBRef
	private CartUser user;
	
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	public Date getCreationTime() {
		return creationTime;
	}
	public void setCreationTime(Date creationTime) {
		this.creationTime = creationTime;
	}
	public Date getLoginTime() {
		return loginTime;
	}
	public void setLoginTime(Date loginTime) {
		this.loginTime = loginTime;
	}
	public Date getLogoutTime() {
		return logoutTime;
	}
	public void setLogoutTime(Date logoutTime) {
		this.logoutTime = logoutTime;
	}
	public CartUser getUser() {
		return user;
	}
	public void setUser(CartUser user) {
		this.user = user;
	}

}
