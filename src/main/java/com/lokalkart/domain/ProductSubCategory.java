package com.lokalkart.domain;

import java.io.Serializable;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * 
 * @author Rohit Ranjan
 *
 */
@Document
public class ProductSubCategory implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6621278370202840696L;
	
	@Id
	private String subCatId;
	
	@Indexed
	private String subCategoryName;
	
	@DBRef
	private List<AppImage> productSubCategoryImage;
	
	@DBRef
	private Set<Brand> brand;

	public Set<Brand> getBrand() {
		if(null == brand){
			brand = new LinkedHashSet<Brand>();
		}
		return brand;
	}

	public void setBrand(Set<Brand> brand) {
		this.brand = brand;
	}

	public String getSubCategoryName() {
		return subCategoryName;
	}

	public void setSubCategoryName(String subCategoryName) {
		this.subCategoryName = subCategoryName;
	}

	public List<AppImage> getProductSubCategoryImage() {
		return productSubCategoryImage;
	}

	public void setProductSubCategoryImage(List<AppImage> productSubCategoryImage) {
		this.productSubCategoryImage = productSubCategoryImage;
	}

	public String getSubCatId() {
		return subCatId;
	}

	public void setSubCatId(String subCatId) {
		this.subCatId = subCatId;
	}

}
