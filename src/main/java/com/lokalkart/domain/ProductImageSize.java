package com.lokalkart.domain;

/**
 * 
 * @author Rohit Ranjan
 *
 */
public enum ProductImageSize {

	SMALL, LARGE, NORMAL, MEDIUM;
}
