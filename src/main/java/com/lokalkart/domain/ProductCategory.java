package com.lokalkart.domain;

import java.io.Serializable;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * 
 * @author Rohit Ranjan
 *
 */
@Document
public class ProductCategory implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8137663642887931267L;
	
	public ProductCategory(){
		
	}

	@Id
	private String catId;
	
	@Indexed(unique = true)
	private String categoryName;
	
	@DBRef
	private List<AppImage> productCategoryImage;
	
	@DBRef
	private Set<ProductSubCategory> subCategories;

	public String getCatId() {
		return catId;
	}

	public void setCatId(String catId) {
		this.catId = catId;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public List<AppImage> getProductCategoryImage() {
		return productCategoryImage;
	}

	public void setProductCategoryImage(List<AppImage> productCategoryImage) {
		this.productCategoryImage = productCategoryImage;
	}

	public Set<ProductSubCategory> getSubCategories() {
		if(null == subCategories){
			subCategories = new LinkedHashSet<ProductSubCategory>();
		}
		return subCategories;
	}

	public void setSubCategories(Set<ProductSubCategory> subCategories) {
		this.subCategories = subCategories;
	}
	

}
