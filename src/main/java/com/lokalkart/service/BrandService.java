package com.lokalkart.service;

import com.lokalkart.domain.Brand;

public interface BrandService {

	Brand saveBrand(Brand brand);
}
