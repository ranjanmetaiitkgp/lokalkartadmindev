package com.lokalkart.service;

import java.util.List;

import com.lokalkart.domain.ProductCategory;

public interface ProductCategoryService{
	
	ProductCategory saveProductCategory(ProductCategory productCategory);
	
	List<ProductCategory> getAllProductCategory();
	
	ProductCategory getProductCategory(String catId);
	
	ProductCategory updateProductCategory(ProductCategory productCategory);

}
