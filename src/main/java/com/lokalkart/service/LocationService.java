package com.lokalkart.service;

import java.util.List;

import com.lokalkart.domain.Location;

public interface LocationService {

	List<Location> getAllLocationByCity(String cityId);
	Location saveLocation(Location location);
}
