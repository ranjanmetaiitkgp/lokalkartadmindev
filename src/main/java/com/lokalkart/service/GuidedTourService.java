package com.lokalkart.service;

import java.util.List;

import com.lokalkart.domain.GuidedTour;
import com.lokalkart.domain.ProductSubCategory;

public interface GuidedTourService{

	List<GuidedTour> getGuidedTourForProductSubCategory(ProductSubCategory productSubCategory);
}
