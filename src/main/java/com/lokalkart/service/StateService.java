package com.lokalkart.service;

import java.util.List;

import com.lokalkart.domain.State;

public interface StateService {
	
	List<State> getAllStateByCountry(String countryId);
	State saveState(State state);
	State getStateById(String stateId);

}
