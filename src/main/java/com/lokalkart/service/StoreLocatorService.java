package com.lokalkart.service;

import com.lokalkart.domain.Address;

public interface StoreLocatorService{

	Address saveAddress(Address address);
}
