package com.lokalkart.service;

import java.util.List;

import com.lokalkart.domain.Product;
import com.lokalkart.domain.Retailer;

public interface RetailerService{
	
	Retailer saveRetailer(Retailer retailer);
	
	List<Retailer> getAllRetailerForProductByLocation(Product product, String pinCode);
	
	Retailer findByRetailerId(String retailerId);
	
}
