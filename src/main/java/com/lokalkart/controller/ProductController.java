package com.lokalkart.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.lokalkart.domain.AppImage;
import com.lokalkart.domain.Product;
import com.lokalkart.domain.jsonelement.AppImageJSON;
import com.lokalkart.domain.jsonelement.ProductJSON;
import com.lokalkart.service.AppImageService;
import com.lokalkart.service.ProductService;

@RestController
@RequestMapping("/product")
public class ProductController {
	
	@Autowired
	private ProductService productService;
	
	@Autowired
	private AppImageService appImageService;

	@RequestMapping(value = "/saveProduct",  method = RequestMethod.POST, produces = {"application/json","application/xml"})
	public Product saveProduct(@RequestBody ProductJSON productJson){
		
		Product product = new Product();
		product.setProductName(productJson.getProductName());
		product.setProductDescription(productJson.getProductDescription());
		product.setItemCode(productJson.getItemCode());
		product.setMaxRetailPrice(productJson.getMaxRetailPrice());
		product.setDeliveryCharge(productJson.getDeliveryCharge());
		product.setDeliveryTime(productJson.getDeliveryTime());
		product.setDiscountPercentage(productJson.getDiscountPercentage());
		product.setKeyFeatures(productJson.getKeyFeatures());
		product.setPaymentTypes(productJson.getPaymentTypes());
		product.setSellingPrice(productJson.getSellingPrice());
		List<AppImageJSON> appImageJsonList = productJson.getProductImages();
		List<AppImage> appImageList = new ArrayList<>();
		if(appImageJsonList != null && !appImageJsonList.isEmpty()){
			for(AppImageJSON appImageJson : appImageJsonList){
				AppImage appImage = new AppImage();
				appImage.setImageSize(appImageJson.getImageSize());
				appImage.setImageUrl(appImageJson.getImageUrl());
				appImage = appImageService.saveAppImage(appImage);
				appImageList.add(appImage);
			}
		}
		product.setProductImages(appImageList);
		
		/*Retailer retailer = retailerService.findByRetailerId(productJson.getRetailerId());
		product.setRetailer(retailer);*/
		
		return productService.saveProduct(product);
	}
}
