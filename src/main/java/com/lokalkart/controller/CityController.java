package com.lokalkart.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.lokalkart.domain.City;
import com.lokalkart.domain.State;
import com.lokalkart.domain.jsonelement.CityJSON;
import com.lokalkart.service.CityService;
import com.lokalkart.service.StateService;

@RestController
@RequestMapping("/city")
public class CityController {
	
	@Autowired
	private CityService cityService;
	
	@Autowired
	private StateService stateService;
	
	@RequestMapping(value = "/saveCity",  method = RequestMethod.POST, produces = {"application/json","application/xml"})
	public City saveCity(@RequestBody CityJSON cityJson){
		City city = new City();
		city.setCityName(cityJson.getCityName());
		State state = stateService.getStateById(cityJson.getStateId());
		city.setState(state);
		return cityService.saveCity(city);
	}
	
	@RequestMapping(value = "/getAllCity",  method = RequestMethod.GET, produces = {"application/json","application/xml"})
	public List<City> getAllCity(){
		return cityService.getAllCity();
	}

}
