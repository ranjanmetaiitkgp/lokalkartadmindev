package com.lokalkart.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.lokalkart.domain.City;
import com.lokalkart.domain.Location;
import com.lokalkart.domain.jsonelement.LocationJSON;
import com.lokalkart.service.CityService;
import com.lokalkart.service.LocationService;

@RestController
@RequestMapping("/location")
public class LocationController {
	
	@Autowired
	private LocationService locationService;
	
	@Autowired
	private CityService cityService;
	
	@RequestMapping(value = "/saveLocation",  method = RequestMethod.POST, produces = {"application/json","application/xml"})
	public Location saveLocation(@RequestBody LocationJSON locationlocJson){
		Location location = new Location();
		location.setLocationName(locationlocJson.getLocationName());
		City city = cityService.getCityById(locationlocJson.getCityId());
		location.setCity(city);
		return locationService.saveLocation(location);
	}
	
	@RequestMapping(value = "/getLocations/{cityId}",  method = RequestMethod.GET, produces = {"application/json","application/xml"})
	public List<Location> getLocationByCity(@PathVariable("cityId") String cityId){
		return locationService.getAllLocationByCity(cityId);
	}

}
