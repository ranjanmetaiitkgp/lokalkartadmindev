package com.lokalkart.controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.lokalkart.domain.AppImage;
import com.lokalkart.domain.Brand;
import com.lokalkart.domain.Product;
import com.lokalkart.domain.ProductCategory;
import com.lokalkart.domain.ProductSubCategory;
import com.lokalkart.domain.jsonelement.AppImageJSON;
import com.lokalkart.domain.jsonelement.BrandJSON;
import com.lokalkart.domain.jsonelement.ProductCategoryJSON;
import com.lokalkart.domain.jsonelement.ProductJSON;
import com.lokalkart.domain.jsonelement.ProductSubCategoryJSON;
import com.lokalkart.service.AppImageService;
import com.lokalkart.service.ProductCategoryService;

@RestController
@RequestMapping("/productCategory")
public class ProductCategoryController {
	
	@Autowired
	private ProductCategoryService productCategoryService;
	
	@Autowired
	private AppImageService appImageService;
	
	@RequestMapping(value = "/saveProductCategory",  method = RequestMethod.POST, produces = {"application/json","application/xml"})
	public @ResponseBody ProductCategory saveProductCategory(@RequestBody ProductCategoryJSON productCategoryJson){
		ProductCategory productCategory = new ProductCategory();
		productCategory.setCategoryName(productCategoryJson.getCategoryName());
		List<AppImageJSON> imageJsonList = productCategoryJson.getProductCategoryImage();
		List<AppImage> images = new ArrayList<>();
		if(imageJsonList !=null && !imageJsonList.isEmpty()){
			for(AppImageJSON appImageJSON : imageJsonList){
				AppImage image = new AppImage();
				image.setImageUrl(appImageJSON.getImageUrl());
				image.setImageSize(appImageJSON.getImageSize());
				image = appImageService.saveAppImage(image);
				images.add(image);
			}
		}
		
		productCategory.setProductCategoryImage(images);
		
		Set<ProductSubCategoryJSON> productSubCategoryJsonList = productCategoryJson.getSubCategories();
		Set<ProductSubCategory> productSubCategoryList = new LinkedHashSet<>();
		if(productSubCategoryJsonList != null && !productSubCategoryJsonList.isEmpty()){
			for(ProductSubCategoryJSON productSubCategoryJSON : productSubCategoryJsonList){
				ProductSubCategory productSubCategory = new ProductSubCategory();
				productSubCategory.setSubCategoryName(productSubCategoryJSON.getSubCategoryName());
				productSubCategory.setSubCatId(productSubCategoryJSON.getSubCatId());
				
				Set<Brand> brands = new LinkedHashSet<Brand>();
				Set<Product> prods = null;
				Iterator<BrandJSON> brandIt = productSubCategoryJSON.getBrand().iterator();
				while(brandIt.hasNext()){
					BrandJSON brandJSON = (BrandJSON) brandIt.next();
					Brand brand = new Brand();
					brand.setBrandId(brandJSON.getBrandId());
					brand.setBrandName(brandJSON.getBrandName());
					prods = new LinkedHashSet<Product>();
					Iterator<ProductJSON> prodIt = brandJSON.getProducts().iterator();
					while(prodIt.hasNext()){
						ProductJSON productJSON = (ProductJSON) prodIt.next();
						Product product = new Product();
						product.setProductId(productJSON.getProductId());
						product.setProductName(productJSON.getProductName());
						prods.add(product);
					}
					brand.setProducts(prods);
					brands.add(brand);
				}
				productSubCategory.setBrand(brands);
				
				List<AppImageJSON> appImageJsonList = productSubCategoryJSON.getProductSubCategoryImage();
				List<AppImage> appImages = new ArrayList<>();
				if(appImageJsonList != null && !appImageJsonList.isEmpty()){
					for(AppImageJSON appImageJSON : appImageJsonList){
						AppImage image = new AppImage();
						image.setImageUrl(appImageJSON.getImageUrl());
						image.setImageSize(appImageJSON.getImageSize());
						image = appImageService.saveAppImage(image);
						appImages.add(image);
						
					}
				}
				productSubCategory.setProductSubCategoryImage(appImages);
				//productSubCategory = productSubCategoryService.saveProductSubCategory(productSubCategory);
				productSubCategoryList.add(productSubCategory);
			}
		}
		
		productCategory.setSubCategories(productSubCategoryList);
		return  productCategoryService.saveProductCategory(productCategory);
	}
	
	@RequestMapping(value = "/getAllProductCategory",  method = RequestMethod.POST, produces = {"application/json","application/xml"})
	public List<ProductCategory> getAllProductCategory(){
		List<ProductCategory> categories = new LinkedList<ProductCategory>();
		categories =  productCategoryService.getAllProductCategory();
		return categories;
	}
	
	@RequestMapping(value = "/getProductCategoryById",  method = RequestMethod.POST, produces = {"application/json","application/xml"})
	public ProductCategory getProductCategoryByName(@RequestBody ProductCategoryJSON productCategoryJson){
		ProductCategory productCategory =  productCategoryService.getProductCategory("5662ca8f33829e6170953b66");
		return productCategory;
	}
	
	@RequestMapping(value = "/updateProductCategory",  method = RequestMethod.POST, produces = {"application/json","application/xml"})
	public ProductCategory updateProductCategory(@RequestBody ProductCategoryJSON productCategoryJson){
		ProductCategory productCategory =  productCategoryService.getProductCategory(productCategoryJson.getCatId());
		Set<ProductSubCategory> productSubCategories = productCategory.getSubCategories();
		Iterator<ProductSubCategory> it = productSubCategories.iterator();
		Set<ProductSubCategoryJSON> productSubCategoryJSONs = productCategoryJson.getSubCategories();
		Iterator<ProductSubCategoryJSON> jsonIt = productSubCategoryJSONs.iterator();
		
		boolean breakFromLoop = false;
		Set<ProductSubCategory> temp = productCategory.getSubCategories();
		while(it.hasNext()){
			if(breakFromLoop){
				break;
			}
			ProductSubCategory productSubCat = it.next();
			while(jsonIt.hasNext()){/*
				ProductSubCategoryJSON productSubCategoryJSON = jsonIt.next();
				ProductSubCategory productSubCategory = new ProductSubCategory();
				if(!productSubCat.getSubCategoryName().equals(productSubCategoryJSON.getSubCategoryName())){
					for(ProductJSON prod: productSubCategoryJSON.getProducts()){
						Product product = new Product();
						product.setProductName(prod.getProductName());
						product.setProductId(prod.getProductId());
						productSubCategory.getProducts().add(product);
					}
					productSubCategory.setSubCategoryName(productSubCategoryJSON.getSubCategoryName());
					productSubCategory.setSubCatId(productSubCategoryJSON.getSubCatId());
					temp.add(productSubCategory);
					breakFromLoop = true;
					break;
				}
			*/}
		}
		productCategory.setSubCategories(temp);
		return  productCategoryService.saveProductCategory(productCategory);
	}
			
		

}
