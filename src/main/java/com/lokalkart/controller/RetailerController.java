package com.lokalkart.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.lokalkart.domain.Address;
import com.lokalkart.domain.Retailer;
import com.lokalkart.domain.jsonelement.AddressJSON;
import com.lokalkart.domain.jsonelement.RetailerJSON;
import com.lokalkart.service.RetailerService;
import com.lokalkart.service.StoreLocatorService;

@RestController
@RequestMapping("/retailer")
public class RetailerController {
	
	@Autowired
	private RetailerService retailerService;
	
	@Autowired
	private StoreLocatorService storeLocatorService;

	@RequestMapping(value = "/saveRetailer",  method = RequestMethod.POST, produces = {"application/json","application/xml"})
	public Retailer saveRetailer(@RequestBody RetailerJSON retailerJson){
		Retailer retailer = new Retailer();
		retailer.setRetailerName(retailerJson.getRetailerName());
		List<AddressJSON> addressJsonList = retailerJson.getAddress();
		List<Address> addressList = new ArrayList<>();
		if(addressJsonList !=null && !addressJsonList.isEmpty()){
			for(AddressJSON addressJson : addressJsonList){
				Address address = new Address();
				/*address.setCity(addressJson.getCity());
				address.setContactNumber(addressJson.getContactNumber());
				address.setCountry(addressJson.getCountry());
				address.setFirstLine(addressJson.getFirstLine());
				address.setPinCode(addressJson.getPinCode());
				address.setSecondLine(addressJson.getSecondLine());
				address.setState(addressJson.getState());
				address = storeLocatorService.saveAddress(address);*/
				addressList.add(address);
			}
		}
		
		retailer.setAddress(addressList);
		return retailerService.saveRetailer(retailer);
	}
}
