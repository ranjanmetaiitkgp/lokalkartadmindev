package com.lokalkart.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.lokalkart.domain.Country;

public interface CountryRepository extends MongoRepository<Country, String>{

}
