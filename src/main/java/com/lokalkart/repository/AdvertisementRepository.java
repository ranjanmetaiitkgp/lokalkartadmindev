package com.lokalkart.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.lokalkart.domain.Advertisement;

public interface AdvertisementRepository extends MongoRepository<Advertisement, String>{

}
