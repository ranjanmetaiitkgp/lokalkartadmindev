package com.lokalkart.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.lokalkart.domain.Address;

public interface StoreLocatorRepository extends MongoRepository<Address, String>{

}
