package com.idearealty.spring.webapp.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.idearealty.spring.model.Circle;
import com.idearealty.spring.service.CircleManager;

@Controller
@RequestMapping("/circleform*")
public class CircleController extends BaseFormController{
	
	public CircleController(){
		setCancelView("redirect:/home");
		setSuccessView("redirect:/circleform");
	}
	
	private CircleManager circleManager;
	
	@Autowired
	public void setCircleManager(CircleManager circleManager) {
		this.circleManager = circleManager;
	}

	
	@RequestMapping(method = RequestMethod.GET)
	public Circle showForm(Model model){
		model.addAttribute("circleList", circleManager.getAllCircles());
		return new Circle();
		
	}
	
	@RequestMapping(method = RequestMethod.POST)
	public String onSubmit(@ModelAttribute("circle") Circle circle, BindingResult errors, HttpServletRequest request, HttpServletResponse response){
		
		if (request.getParameter("cancel") != null) {
            return getCancelView();
        }
		
		if (validator != null) { // validator is null during testing
            validator.validate(circle, errors);

            if (errors.hasErrors()) {
                return "circleform";
            }
        }
		
		circleManager.saveCircle(circle);
		return getSuccessView();
	}
}
