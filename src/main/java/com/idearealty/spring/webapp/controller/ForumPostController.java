package com.idearealty.spring.webapp.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.idearealty.spring.model.ForumPost;
import com.idearealty.spring.model.User;
import com.idearealty.spring.service.ForumManager;
import com.idearealty.spring.service.ForumPostManager;
import com.idearealty.spring.service.UserManager;

@Controller
@RequestMapping("/forumpostform*")
public class ForumPostController extends BaseFormController {
	
	public ForumPostController(){
		setCancelView("redirect:/forumform");
		setSuccessView("redirect:/forumpostform");
	}
	
	private ForumPostManager forumPostManager;
	
	private ForumManager forumManager;
	
	private UserManager userManager;

	
	@Autowired
	public void setUserManager(UserManager userManager) {
		this.userManager = userManager;
	}

	@Autowired
	public void setForumPostManager(ForumPostManager forumPostManager) {
		this.forumPostManager = forumPostManager;
	}
	
	@Autowired
	public void setForumManager(ForumManager forumManager) {
		this.forumManager = forumManager;
	}
	
	@ModelAttribute
	@RequestMapping(method = RequestMethod.GET)
	public ForumPost showForm(Model model, HttpServletRequest request, HttpServletResponse response){
		String forumId = request.getParameter("id");
		model.addAttribute("forumPostList", forumPostManager.getAllPostsForForum(new Long (forumId)));
		return new ForumPost();
		
	}
	
	@RequestMapping(method = RequestMethod.POST)
	public String onSubmit(@ModelAttribute("forumPost") ForumPost forumPost, BindingResult errors, HttpServletRequest request, HttpServletResponse response){
		
		if (request.getParameter("cancel") != null) {
            return getCancelView();
        }
		
		if (validator != null) { // validator is null during testing
            validator.validate(forumPost, errors);

            if (errors.hasErrors()) {
                return getCancelView();
            }
        }
		
		String forumId = request.getParameter("id");
		forumPost.setForum(forumManager.getForumById(new Long(forumId)));
		forumPost.setCreationDate(new Date());
		User currentUser = userManager.getUserByUsername(request.getRemoteUser());
		forumPost.setContentWriter(currentUser);
		forumPostManager.saveForumPost(forumPost);
		return getSuccessView() + "?id=" + forumId;
	}

}
