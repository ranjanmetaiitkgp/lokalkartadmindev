package com.idearealty.spring.webapp.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.idearealty.spring.model.Circle;
import com.idearealty.spring.model.Forum;
import com.idearealty.spring.service.ForumManager;

@Controller
@RequestMapping("/forumform*")
public class ForumController extends BaseFormController{
	
	public ForumController() {
		setCancelView("redirect:/home");
		setSuccessView("redirect:/forumform");
	}
	
	private ForumManager forumManager;

	@Autowired
	public void setForumManager(ForumManager forumManager) {
		this.forumManager = forumManager;
	}
	
	@ModelAttribute
	@RequestMapping(method = RequestMethod.GET)
	public Forum showForm(Model model){
		model.addAttribute("forumList", forumManager.getAllForums());
		return new Forum();
		
	}
	
	@RequestMapping(method = RequestMethod.POST)
	public String onSubmit(@ModelAttribute("forum") Forum forum, BindingResult errors, HttpServletRequest request, HttpServletResponse response){
		
		if (request.getParameter("cancel") != null) {
            return getCancelView();
        }
		
		if (validator != null) { // validator is null during testing
            validator.validate(forum, errors);

            if (errors.hasErrors()) {
                return "forumform";
            }
        }
		
		forumManager.saveForum(forum);
		return getSuccessView();
	}

}
