package com.idearealty.spring.dao;

import java.util.List;

import com.idearealty.spring.model.ForumPost;

public interface ForumPostDao extends GenericDao<ForumPost, Long>{
	
	String GET_ALL_FORUM_POSTS = "select fp from ForumPost fp join fp.forum forum where forum.id = :forumId ";
	
	List<ForumPost> getAllPostsForForum(Long forumId);

}
