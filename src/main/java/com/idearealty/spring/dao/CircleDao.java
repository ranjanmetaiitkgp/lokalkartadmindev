package com.idearealty.spring.dao;

import java.util.List;

import com.idearealty.spring.model.Circle;

public interface CircleDao extends GenericDao<Circle, Long>{
	
	String GET_ALL_CIRCLES = "from Circle circle";
	
	List<Circle> getAllCircles();

}
