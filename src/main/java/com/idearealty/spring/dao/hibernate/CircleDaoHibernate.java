package com.idearealty.spring.dao.hibernate;

import java.util.List;

import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.idearealty.spring.dao.CircleDao;
import com.idearealty.spring.model.Circle;

@Repository("circleDao")
public class CircleDaoHibernate extends GenericDaoHibernate<Circle, Long> implements CircleDao{

	public CircleDaoHibernate() {
		super(Circle.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Circle> getAllCircles() {
		return getSession().createQuery(GET_ALL_CIRCLES).list();
	}

}
