package com.idearealty.spring.dao.hibernate;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.idearealty.spring.dao.ForumPostDao;
import com.idearealty.spring.model.ForumPost;

@Repository("forumPostDao")
public class ForumPostDaoHibernate extends GenericDaoHibernate<ForumPost, Long> implements ForumPostDao{

	public ForumPostDaoHibernate() {
		super(ForumPost.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ForumPost> getAllPostsForForum(Long forumId) {
		Query query = getSession().createQuery(GET_ALL_FORUM_POSTS);
		query.setLong("forumId", forumId);
		return query.list();
	}

}
