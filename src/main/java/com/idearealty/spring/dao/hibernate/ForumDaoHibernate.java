package com.idearealty.spring.dao.hibernate;


import org.springframework.stereotype.Repository;

import com.idearealty.spring.dao.ForumDao;
import com.idearealty.spring.model.Forum;

@Repository("forumDao")
public class ForumDaoHibernate extends GenericDaoHibernate<Forum, Long> implements ForumDao{

	public ForumDaoHibernate() {
		super(Forum.class);
	}

}
