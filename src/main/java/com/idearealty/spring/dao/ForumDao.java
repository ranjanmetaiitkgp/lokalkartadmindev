package com.idearealty.spring.dao;

import com.idearealty.spring.model.Forum;

public interface ForumDao extends GenericDao<Forum, Long>{

}
