package com.idearealty.spring.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="forum_post")
public class ForumPost implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6133046431525897558L;
	
	private Long postId;
	private String content;
	private Forum forum;
	private Date creationDate;
	private User contentWriter;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Long getPostId() {
		return postId;
	}
	public void setPostId(Long postId) {
		this.postId = postId;
	}
	
	@Column(nullable = false, length = 255)
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	
	@ManyToOne(targetEntity=com.idearealty.spring.model.Forum.class, fetch=FetchType.EAGER)
	public Forum getForum() {
		return forum;
	}
	public void setForum(Forum forum) {
		this.forum = forum;
	}
	
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	
	@OneToOne(targetEntity=com.idearealty.spring.model.User.class,fetch=FetchType.EAGER)
	public User getContentWriter() {
		return contentWriter;
	}
	public void setContentWriter(User contentWriter) {
		this.contentWriter = contentWriter;
	}
}
