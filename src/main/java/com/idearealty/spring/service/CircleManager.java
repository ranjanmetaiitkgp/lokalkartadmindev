package com.idearealty.spring.service;

import java.util.List;

import com.idearealty.spring.model.Circle;

public interface CircleManager extends GenericManager<Circle, Long>{
	
	Circle saveCircle(Circle circle);
	List<Circle> getAllCircles();

}
