package com.idearealty.spring.service;

import java.util.List;

import com.idearealty.spring.model.Forum;

public interface ForumManager extends GenericManager<Forum, Long>{
	
	Forum saveForum(Forum forum);
	Forum getForumById(Long forumId);
	List<Forum> getAllForums();

}
