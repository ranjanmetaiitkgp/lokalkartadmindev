package com.idearealty.spring.service;

import java.util.List;

import com.idearealty.spring.model.ForumPost;

public interface ForumPostManager extends GenericManager<ForumPost, Long>{
	
	ForumPost saveForumPost(ForumPost post);
	List<ForumPost> getAllPostsForForum(Long forumId);

}
