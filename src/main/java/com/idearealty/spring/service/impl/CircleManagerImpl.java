package com.idearealty.spring.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.idearealty.spring.dao.CircleDao;
import com.idearealty.spring.model.Circle;
import com.idearealty.spring.service.CircleManager;

@Service("circleManager")
public class CircleManagerImpl extends GenericManagerImpl<Circle, Long> implements CircleManager{
	
	private CircleDao circleDao;

	@Override
	public Circle saveCircle(Circle circle) {
		return circleDao.save(circle);
	}

	@Autowired
	public void setCircleDao(CircleDao circleDao) {
		this.circleDao = circleDao;
	}

	@Override
	public List<Circle> getAllCircles() {
		return circleDao.getAll();
	}

}
