package com.idearealty.spring.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.idearealty.spring.dao.ForumPostDao;
import com.idearealty.spring.model.ForumPost;
import com.idearealty.spring.service.ForumPostManager;

@Service("forumPostManager")
public class ForumPostManagerImpl extends GenericManagerImpl<ForumPost, Long> implements ForumPostManager{
	
	private ForumPostDao forumPostDao;
	
	@Autowired
	public void setForumPostDao(ForumPostDao forumPostDao) {
		this.forumPostDao = forumPostDao;
	}

	@Override
	public ForumPost saveForumPost(ForumPost post) {
		return forumPostDao.save(post);
	}

	@Override
	public List<ForumPost> getAllPostsForForum(Long forumId) {
		return forumPostDao.getAllPostsForForum(forumId);
	}

}
