package com.idearealty.spring.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.idearealty.spring.dao.ForumDao;
import com.idearealty.spring.model.Forum;
import com.idearealty.spring.service.ForumManager;

@Service("forumManager")
public class ForumManagerImpl extends GenericManagerImpl<Forum, Long> implements ForumManager{
	
	private ForumDao forumDao;

	@Override
	public Forum saveForum(Forum forum) {
		return forumDao.save(forum);
	}

	@Override
	public List<Forum> getAllForums() {
		return forumDao.getAll();
	}

	@Autowired
	public void setForumDao(ForumDao forumDao) {
		this.forumDao = forumDao;
	}

	@Override
	public Forum getForumById(Long forumId) {
		return forumDao.get(forumId);
	}

}
